CC = gcc
BIN = stringart
VPATH = src

.PHONY: all clean
all: $(BIN)

debug: CFLAGS += -g
debug: $(BIN)

clean:
	$(RM) $(BIN)
